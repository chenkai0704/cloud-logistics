package com.lude.edi.pojo.protocol;

import org.apache.http.HttpEntity;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class OrderExample
{
    static Logger logger = LoggerFactory.getLogger(ClientCustomSSL.class);

    public final static void main(String[] args) throws Exception
    {

        String str = "{\"PreOrderNo\":\"2016080317222300111\",\"AppTime\":\"20160803172253\",\"EbcCode\":\"3122448937\",\"GoodsValue\":524.00,\"TaxFee\":65.93,\"Freight\":30.00,\"UnderTheSingerName\":\"姜贝贝\",\"UnderTheSingerCode\":\"371083198707248029\",\"UnderTheSingerTel\":\"18563105558\",\"UnderTheSingerAddress\":\"山东威海市环翠区渔港路38号恒丰银行东富民村镇银行\",\"Consignee\":\"姜贝贝\",\"ConsigneeCode\":\"371083198707248029\",\"ConsigneeAddress\":\"山东威海市环翠区渔港路38号恒丰银行东富民村镇银行\",\"ConsigneeTelephone\":\"18563105558\",\"LogisticsCode\":\"3120980105\",\"LogisticsName\":\"上海韵达货运有限公司\",\"LogisticsNo\":\"5530044551719\",\"Note\":\"这里填一些备注内容\",\"OrderList\":[{\"GNum\":\"1\",\"ItemNo\":\"ECS002331\",\"Unit\":\"罐\",\"Qty\":2,\"Price\":108.00,\"Total\":216.00,\"Wet\":5.70,\"Note\":\"\"},{\"GNum\":\"2\",\"ItemNo\":\"ECS002331\",\"Unit\":\"罐\",\"Qty\":2,\"Price\":108.00,\"Total\":216.00,\"Wet\":5.70,\"Note\":\"\"}]}";
        CloseableHttpClient httpclient = HttpClients.createDefault();
        try {
            List<BasicNameValuePair> params = new ArrayList<>();
            BasicNameValuePair pappkey = new BasicNameValuePair("appkey", "dd1e3a22cbfe44e79afdd5ea0d6a245e");
            params.add(pappkey);
            BasicNameValuePair pcode = new BasicNameValuePair("code", "301");
            params.add(pcode);
            BasicNameValuePair pdata = new BasicNameValuePair("data", str);
            params.add(pdata);
            HttpEntity httpEntity = new UrlEncodedFormEntity(params, "UTF-8");

            HttpPost httpPost = new HttpPost("http://180.167.77.174:8082/edi/allOrderMssageReceiver");
            httpPost.setEntity(httpEntity);
            logger.debug("执行请求 {} ", httpPost.getRequestLine());

            CloseableHttpResponse response = httpclient.execute(httpPost);
            try {
                HttpEntity entity = response.getEntity();

                logger.debug("----------------------------------------");
                logger.debug(response.getStatusLine().toString());
               logger.debug("{}", EntityUtils.toString(entity));
            } finally {
                response.close();
            }
        } finally {
            httpclient.close();
        }
    }

}