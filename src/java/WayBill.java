package com.lude.edi.pojo.protocol;

import org.apache.http.HttpEntity;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class OrderExample
{
    static Logger logger = LoggerFactory.getLogger(OrderExample.class);

    public final static void main(String[] args) throws Exception
    {

       String str = "{\"LogisticsNo\":\"5530005731719\",\"OrderNo\":\"2016121900001\",\"AppTime\":\"20160805143142\",\"LogisticsCode\":\"320596T002\",\"LogisticsName\":\"苏州申通物流有限公司\",\"IEFlag\":\"I\",\"TrafMode\":\"5\",\"TrafName\":\"航空运输\",\"VoyageNo\":\"CA9999-CA2123\",\"BillNo\":\"45145170\",\"FromCountryName\":\"日本\",\"Freight\":20.00,\"InsuredFee\":0,\"Weight\":5.70,\"Netwt\":5.60,\"PackNo\":2,\"GoodsInfo\":\"布莱米尔婴儿奶粉3段1200G,阿米龙婴儿奶粉3段800g,\",\"ReceiveProvince\":\"江苏\",\"ReceiveCity\":\"苏州\",\"ReceiveArea\":\"相城区澄波路509号\",\"Consignee\":\"姜贝贝\",\"ConsigneeAddress\":\"江苏省苏州市相城区澄波路509号\",\"ConsigneeTelephone\":\"18563105558\",\"ConsigneeCountry\":\"142\",\"Shipper\":\"FUBEST\",\"ShipperTelephone\":\"911728752\",\"ShipperCountry\":\"312\",\"SendProvince\":\"大阪\",\"SendCity\":\"大阪\",\"SendArea\":\"大阪\",\"ShipperAddress\":\"日本大阪\",\"Note\":\"无\",\"MainBillNo\":\"45145170\",\"InOutPortCode\":\"31015005\",\"DeclarePortCode\":\"31015005\",\"ArrivedPortCode\":\"31015005\",\"PackTypeCode\":\"4M\"}";
       CloseableHttpClient httpclient = HttpClients.createDefault();
        try {
            List<BasicNameValuePair> params = new ArrayList<>();
            BasicNameValuePair pappkey = new BasicNameValuePair("appkey", "dd1e3a22cbfe44e79afdd5ea0d6a245e");
            params.add(pappkey);
            BasicNameValuePair pcode = new BasicNameValuePair("code", "501");
            params.add(pcode);
            BasicNameValuePair pdata = new BasicNameValuePair("data", str);
            params.add(pdata);
            HttpEntity httpEntity = new UrlEncodedFormEntity(params, "UTF-8");

            HttpPost httpPost = new HttpPost("http://180.167.77.174:8082/edi/allOrderMssageReceiver");
            httpPost.setEntity(httpEntity);
            logger.debug("执行请求 {} ", httpPost.getRequestLine());

            CloseableHttpResponse response = httpclient.execute(httpPost);
            try {
                HttpEntity entity = response.getEntity();

                logger.debug("----------------------------------------");
                logger.debug(response.getStatusLine().toString());
                logger.debug("{}", EntityUtils.toString(entity));
            } finally {
                response.close();
            }
        } finally {
            httpclient.close();
        }
    }

}